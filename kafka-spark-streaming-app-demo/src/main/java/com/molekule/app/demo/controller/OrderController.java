package com.molekule.app.demo.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/admin/orders")
public class OrderController {
	private static final Logger log = LogManager.getLogger(OrderController.class);
	
	private Integer errorCount = 0;
	
	@PostMapping
	public void createOrder(@RequestBody String order) {
		if("error".equalsIgnoreCase(order)) {
			errorCount++;
			if(errorCount <= 3) {
				log.error("Error count: {}", errorCount);
				throw new RuntimeException("This is an error");
			} else {
				errorCount = 0;
			}
		}
		
		log.info("Created order: {}", order);
	}

}
