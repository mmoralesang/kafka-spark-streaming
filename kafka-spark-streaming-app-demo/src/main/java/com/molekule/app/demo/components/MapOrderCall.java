package com.molekule.app.demo.components;

import org.apache.spark.api.java.function.Function;

import com.molekule.app.demo.model.RealTimeOrder;

//@Component
public class MapOrderCall implements Function<String, RealTimeOrder> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4903523751888670899L;

	@Override
	public RealTimeOrder call(String payload) throws Exception {
		RealTimeOrder rto = new RealTimeOrder();
		rto.setOrder(payload);
		return rto;
	}

}