package com.molekule.app.demo.streaming;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.apache.spark.SparkConf;
import org.apache.spark.streaming.Durations;
import org.apache.spark.streaming.api.java.JavaDStream;
import org.apache.spark.streaming.api.java.JavaPairDStream;
import org.apache.spark.streaming.api.java.JavaPairInputDStream;
import org.apache.spark.streaming.api.java.JavaStreamingContext;
import org.apache.spark.streaming.kafka.KafkaUtils;

import com.molekule.app.demo.config.KafkaConfig;

import kafka.serializer.StringDecoder;
import scala.Tuple2;

public class KafkaSparkStreamingCountWords {
	
	public static void main(String []args) {
		Map<String, String> kafkaParams = new HashMap<>();
		kafkaParams.put("bootstrap.servers", "localhost:9092");
		
		Set<String> topics = new HashSet<>(Arrays.asList(KafkaConfig.getTopic()));
		
		SparkConf sparkConf = new SparkConf().setMaster("local[2]").setAppName("KafkaStreamingAppDemo");
		sparkConf.set("spark.streaming.stopGracefullyOnShutdown", "true");

		// create a Spark Java streaming context, with stream batch interval
		JavaStreamingContext jssc = new JavaStreamingContext(sparkConf, Durations.seconds(5));
		
		// Create direct kafka stream with brokers and topics
		JavaPairInputDStream<String, String> stream = 
				KafkaUtils.createDirectStream(
							jssc,
							String.class,
					        String.class,
					        StringDecoder.class,
					        StringDecoder.class,
							kafkaParams, 
							topics
						  ); 
		
		// Get the lines, split them into words, count the words and print
		JavaDStream<String> lines = stream.map(x -> {return x._2;});
		
		JavaDStream<String> words = lines.flatMap(line -> Arrays.asList(line.split(" ")));
		
		// Count each word in each batch
		JavaPairDStream<String, Integer> wordCounts = words.mapToPair(s -> new Tuple2<>(s, 1))
        	.reduceByKey((i1, i2) -> i1 + i2);
		
		
		// Print the first ten elements of each RDD generated in this DStream to the console
		wordCounts.print();
		
		// Start the computation
		jssc.start();
		jssc.awaitTermination();
	}

}
