package com.molekule.app.demo.model;

import java.io.Serializable;

import com.google.gson.Gson;

public class RealTimeOrder implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5227824451857478575L;

	private String order;

	public String getOrder() {
		return order;
	}

	public void setOrder(String order) {
		this.order = order;
	}

	@Override
	public String toString() {
		return new Gson().toJson(this);
	}
	
}
