package com.molekule.app.demo.streaming;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.apache.spark.SparkConf;
import org.apache.spark.streaming.Durations;
import org.apache.spark.streaming.api.java.JavaDStream;
import org.apache.spark.streaming.api.java.JavaPairInputDStream;
import org.apache.spark.streaming.api.java.JavaStreamingContext;
import org.apache.spark.streaming.kafka.KafkaUtils;
import org.springframework.web.client.RestTemplate;

import com.molekule.app.demo.components.MapOrderCall;
import com.molekule.app.demo.config.KafkaConfig;
import com.molekule.app.demo.model.RealTimeOrder;
import com.molekule.app.demo.util.Constants;

import kafka.serializer.StringDecoder;

public class KafkaSparkStreamingPostOrder {
	
	private Map<String, String> kafkaParams = new HashMap<>();
	private Set<String> topics = new HashSet<>(Arrays.asList(KafkaConfig.getTopic()));
	private SparkConf sparkConf = new SparkConf().setMaster("local[2]").setAppName("KafkaStreamingOrderAppDemo");
	
	private JavaStreamingContext jssc; 
	
	//@Autowired
	private MapOrderCall mapOrderCall = new MapOrderCall();;
	
	public KafkaSparkStreamingPostOrder() {
		kafkaParams.put("bootstrap.servers", "localhost:9092");
		
		sparkConf.set("spark.streaming.stopGracefullyOnShutdown", "true");

		// create a Spark Java streaming context, with stream batch interval
		jssc = new JavaStreamingContext(sparkConf, Durations.seconds(30));
		//jssc.checkpoint("E:\\sparkdata\\orders");
		
		// Create direct kafka stream with brokers and topics
		JavaPairInputDStream<String, String> stream = 
				KafkaUtils.createDirectStream(
							jssc,
							String.class,
					        String.class,
					        StringDecoder.class,
					        StringDecoder.class,
							kafkaParams, 
							topics
						  ); 
		
		// Get messages from Kafka
		JavaDStream<String> messages = stream.map(x -> {return x._2;});
		
		// transform to RealTimeOrder objects
		JavaDStream<RealTimeOrder> callData = messages.map(mapOrderCall);
		
		// For each order to create, spark is going to call to the Shopify Order API
		callData.foreachRDD(rdd -> {
			rdd.foreachPartition(partitionOfRecords -> {
				RestTemplate restTemplate = new RestTemplate();
				
				while (partitionOfRecords.hasNext()) {
					RealTimeOrder rto = partitionOfRecords.next();
					restTemplate.postForEntity(Constants.SHOPIFY_ORDER_API, rto.getOrder(), String.class); // Calling to Shopify Order API
				}
				
			});
		});
		
		// Print the first ten elements of each RDD generated in this DStream to the console
		callData.print();
	}
	
	public void start() {
		jssc.start();
		jssc.awaitTermination();
	}

	public static void main(String[] args) {
		KafkaSparkStreamingPostOrder sparkStreamProcessing = new KafkaSparkStreamingPostOrder();
		sparkStreamProcessing.start();
	}


}
