
Example project with *Spark Streaming* order data from Kafka topic to post it to an external API in real time.

Technology Stack

*SpringBoot*
*Spring Integration*
*Kafka*
*Spark Streaming*


Requirement to run-

Kafka; zookeeper up and running;

create a "demoPOC" topic in kafka


1) run the com.molekule.app.demo.streaming.KafkaSparkStreamingPostOrder.java sparking streaming job to listen to kafka topic 
2) run com.molekule.app.demo.DemoApplication.java (Spring boot application) to have a controller listening to post data of incoming orders
3) run com.molekule.app.demo.DemoParser.java to simulate some traffic data(loading from clickstreamdemo.txt) and post it to SpringBoot Demo Controller


***Observe in the KafkaSparkStreamingPostOrder.java, sparking streaming is posting orders to an external API every 30 seconds in real time.
